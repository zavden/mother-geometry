const return_error_value = (val) => {
    if(isNaN(val)) return `${val} is not a number`
    else if(Number(val)<0) return `${val} cannot be negative`
    else return false
}

const read_sides = () => {
    return {
        a : {
            side: false,
            angle: get_value(all_inputs_arr[1].value),
        },
        b : {
            side: false,
            angle: get_value(all_inputs_arr[1].value),
        },
        c : {
            side: get_value(all_inputs_arr[0].value),
            angle: false,
        },
        Area : false,
        h: false,
    }
}

const get_value = (v) => {
    if(v === "")
        return false
    else
        return Number(v)
}

const get_digits = (number) => {
    return number.toString().length;
}

const get_digits_max_5 = (number) => {
    let d = fv(number.toString()).length
    console.log("DIGITS = " + d)
    if(d >= 3) return d
    else  return 0
}

const get_arc_sin_side = (alpha, gamma, c) => {
    console.log(c)
    console.log(Math.sin(alpha))
    console.log(Math.sin(gamma))
    console.log(`c * Math.sin(alpha) / Math.sin(gamma) = ${c * Math.sin(alpha) / Math.sin(gamma)}`)
    return c * Math.sin(alpha) / Math.sin(gamma)
}

const middle_title = () => {
    let mt = document.createElement("p")
    mt.setAttribute("class","middle-title")
    mt.innerHTML = "Gleichschenkliges Dreieck \\(\\rightarrow\\) Höhe = Mittelsenkrechte"
    return mt
}