const sum_angles = (alpha, beta, gamma, color="green") => {
    return `
        \\begin{align*}
        & \\color{${color}}{\\alpha + \\beta + \\bcolor{\\gamma} \\color{${color}}= 180^\\circ} \\\\
        & \\color{${color}}{\\phantom{\\alpha + \\beta + }\\,\\ \\bcolor{\\gamma} \\color{${color}}\\color{${color}}= 180^\\circ - \\centerto{\\alpha}{${fv(alpha)}^\\circ} - \\centerto{\\beta}{${fv(beta)}^\\circ}} \\\\
        & \\phantom{\\alpha + \\beta + }\\,\\ \\gamma \\color{${color}} = 180^\\circ - ${fv(alpha)}^\\circ - ${fv(beta)}^\\circ \\\\
        & \\phantom{\\alpha + \\beta + }\\,\\ \\gamma \\color{${color}} = ${fv(gamma)}^\\circ \\\\
        \\end{align*}
    `
}


const sine_theorem = (a, alpha, c, gamma, color="black") => {
    return `
        \\begin{align*}
        & {\\frac{\\bcolor{a}}{\\sin\\alpha} \\color{${color}}= \\frac{c}{\\sin\\centerto{\\gamma}{(${fv(gamma)}^\\circ)}} }\\\\
        & \\color{${color}}{\\centerto{\\bcolor{a}}{\\frac{a}{\\sin\\alpha}} = \\frac{c}{\\sin\\centerto{\\gamma}{(${fv(gamma)}^\\circ)}} * \\color{black}\\sin\\centerto{\\alpha}{${fv(alpha)}^\\circ} }\\\\
        & \\centerto{a}{\\frac{a}{\\color{${color}}\\sin\\alpha}} \\color{${color}}= \\frac{${fv(c)}}{\\sin(${fv(gamma)}^\\circ)} * \\color{black}\\sin(${fv(alpha)}^\\circ) \\\\
        & \\centerto{a}{\\frac{a}{\\color{${color}}\\sin\\alpha}} \\color{${color}}= ${fv(a)} \\\\
        \\end{align*}
    `
}

const pythagorean_theorem = (h, b, c, color="black") => {
    return `
        \\begin{align*}
        & {h\\color{${color}}^2 + \\left(\\frac{c}{2}\\right)^2 = \\,\\,\\,\\,\\, \\centerto{a^2}{${fv(b)}^2} }\\\\
        & {h\\color{${color}}^2 \\,\\,\\phantom{+ \\left(\\frac{c}{2}\\right)^2} = \\,\\,\\,\\,\\,\\, \\centerto{a^2}{${fv(b)}^2} - \\ \\,\\left(\\frac{c}{2}\\right)^2}\\\\
        & {h\\,\\,\\,\\,\\phantom{+ \\left(\\frac{c}{2}\\right)^2} \\color{${color}}= \\sqrt{\\centerto{a^2}{${fv(b)}^2} - \\ \\,\\left(\\frac{c}{2}\\right)^2}} \\\\
        & h\\,\\,\\,\\,\\phantom{+ \\left(\\frac{c}{2}\\right)^2} \\color{${color}}= \\sqrt{${fv(b)}^2 - \\ \\,\\centerto{${fv(c/2)}^2}{\\left(\\frac{c}{2}\\right)^2}} \\\\
        & h\\,\\,\\,\\,\\phantom{+ \\left(\\frac{c}{2}\\right)^2} \\color{${color}}= \\centerto{${fv(h)}}{\\sqrt{${fv(b)}^2 - \\ \\,\\left(\\frac{${fv(c/2)}}{2}\\right)^2}} \\\\
        
        \\end{align*}
    `
}


const area_solution = (A, c, h, color="black") => {
    return `
    \\begin{align*}
        & {A \\color{${color}}= \\frac{\\centerto{g}{${fv(c)}} * \\centerto{h}{${fv(h)}}}{2}} \\\\[10pt]
        & A \\color{${color}}= \\frac{${fv(c)} * ${fv(h)}}{2} \\\\[5pt]
        & A \\color{${color}}= \\centerto{${fv(A)}}{\\frac{${fv(c)} * ${fv(h)}}{2}} \\\\
    \\end{align*}
    `
}

// & A = \\frac{${fv(c)} * ${fv(h)}{2} \\\\
// & A = ${fv(A)} \\\\