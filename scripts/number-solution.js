const number_solution = SIDES => {
    let
        sides_steps = [],
        alpha = SIDES.a.angle * pi / 180,
        beta = SIDES.b.angle * pi / 180,
        c = SIDES.c.side,
        gamma = pi - alpha - beta,
        a = get_arc_sin_side(alpha, gamma, c),
        b = a,
        h = Math.sqrt(b*b - (c/2)*(c/2)),
        A = c * h / 2
    sides_steps.push(JSON.parse(JSON.stringify(SIDES)))
    SIDES.c.angle = gamma * 180 / pi
    sides_steps.push(JSON.parse(JSON.stringify(SIDES)))
    SIDES.a.side = a
    SIDES.b.side = a
    sides_steps.push(JSON.parse(JSON.stringify(SIDES)))
    SIDES.h = h
    sides_steps.push(JSON.parse(JSON.stringify(SIDES)))
    SIDES.Area = A
    sides_steps.push(JSON.parse(JSON.stringify(SIDES)))
    return sides_steps
}