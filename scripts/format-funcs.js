const format_value = v => String(Number(v)).replace(".",",")

const fv = v => String(Number(Number(v).toFixed(2))).replace(".",",")

window.MathJax = {
    loader: {
        load: [
            '[tex]/mathtools',
            '[tex]/html',
            '[tex]/color'
        ]
    },
    tex: {
        packages: {
            '[+]': [
                'mathtools',
                'html',
                'color'
            ]
        }
    }
}

const pi = Math.PI