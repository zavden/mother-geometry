let
    all_inputs = document.querySelectorAll(".in-value"),
    button  = document.getElementById("init-button"),
    steps_block = document.getElementById("steps-block")

all_inputs_arr = Array.from(all_inputs)


all_inputs.forEach((e, i, arr) => {
    e.addEventListener("keyup", (event) => {
        if (event.defaultPrevented) {}
        if (event.key === "Enter") run_program()
    })
})

button.addEventListener("click",() => run_program())