const tg = (l,t,sides) =>{
    if(sides[l][t]===false) return ""
    if(t==="angle") return `\\mbox{${fv(sides[l][t])}}^\\circ`
    else if(t==="side") return `\\mbox{${fv(sides[l][t])}}`
}

const tm = (l,t,sides) =>{
    if(sides[l][t]===false) return ""
    if(t==="angle") return `\\mbox{${fv(sides[l][t]/2)}}^\\circ`
    else if(t==="side") return `\\mbox{${fv(sides[l][t]/2)}}`
}

const ta = (sides) => {
    if(sides.Area===false) return ""
    else return `\\mbox{${fv(sides.Area)}}`
}

const th = (sides) => {
    if(sides.Area===false) return ""
    else return `\\mbox{${fv(sides.h)}}`
}

const get_tex_to_svg = (x,y,tex,g_class="svg-text", font_size= "2em", color="black") => {
    return `
    <foreignObject x="${x}" y="${y}" width="150" height="80" text-anchor="middle" class="${g_class}" color="${color}">
       <body xmlns="http://www.w3.org/1999/xhtml">
         <div style="font-size: ${font_size};fill-opacity: 1;">
           \\(${tex}\\)
         </div>
       </body>
     </foreignObject>
    `
}

const step_figure_svg = (
        sides,angle_shift=2,angle_radius=20, base_color="blue", horizontal_shift=5,
        gamma_color="black", a_color="black", alpha_color="black", beta_color="black"
    ) => {
    let
        x_shift = 200,
        y_shift = 120,
        A = 380,
        h = 300,
        last_dot_x = h/2.4
    let
        W = A+x_shift,
        H = h+y_shift

    return `
    <svg width="${W}" height="${H}" class="svg-result-step">
          <g transform="translate(${(W-A)/2},${(H-h)/2})">
          <!-- A value--->
          <g transform="translate(0,0)">
            ${get_tex_to_svg(last_dot_x/2-get_digits(fv(sides.c.side))*15 - 30, h/2-20,tg("a","side",sides),undefined,undefined,a_color)}
          </g>
          <!-- B value--->
          <g transform="translate(0,0)">
            ${get_tex_to_svg(A-(A-last_dot_x)/2+50, h/2-20,tg("b","side",sides),undefined,undefined,a_color)}
          </g>
          <!-- C value--->
          ${get_tex_to_svg(A/2-get_digits(fv(sides.c.side))*7.5 - horizontal_shift, h+10,tg("c","side",sides),undefined,undefined,gamma_color)}
          <!-- alpha value--->
          <g transform="translate(-100,-30)">
            ${get_tex_to_svg(A, h,tg("a","angle",sides),undefined,"1.3em",alpha_color)}
          </g>
          <!-- beta value--->
          <g transform="translate(30,-30)">
            ${get_tex_to_svg(0, h,tg("b","angle",sides),undefined,"1.3em",beta_color)}
          </g>
          <!-- gamma value--->
          <g transform="translate(0,0)">
            ${get_tex_to_svg(A/2 - get_digits_max_5(sides.c.angle)*3 - 10, 60,tg("c","angle",sides),undefined,"1.3em",gamma_color)}
          </g>
          <polygon 
                points="0,${h}  ${A},${h} ${A/2},0"
                fill="none" stroke="black" stroke-width="2"
                stroke-linejoin="round"
          />
      </g>
      <rect width="${W}" height="${H}" style="stroke-width:0;stroke:rgb(0,0,0)" fill="none" />
    </svg>
    `
}

// ${get_tex_to_svg(A/2-15, h+10,tg("c","side",sides))}
const step_figure_svg_dashed2 = (
    sides,angle_shift=2,angle_radius=20, base_color="green", horizontal_shift=5,
    gamma_color="black", a_color="black", alpha_color="black", beta_color="black"
) => {
    let
        x_shift = 200,
        y_shift = 120,
        A = 380,
        h = 300,
        last_dot_x = h/2.4
    let
        W = A+x_shift,
        H = h+y_shift


    return `
    <svg width="${W}" height="${H}" class="">
          <g transform="translate(${(W-A)/2},${(H-h)/2})">
          <!-- A value--->
          <g transform="translate(0,0)">
            ${get_tex_to_svg(last_dot_x/2-get_digits(fv(sides.c.side))*15 - 30, h/2-20,tg("a","side",sides),undefined,undefined,a_color)}
          </g>
          <!-- B value--->
          <g transform="translate(0,0)">
            ${get_tex_to_svg(A-(A-last_dot_x)/2+50, h/2-20,tg("b","side",sides),undefined,undefined,a_color)}
          </g>
          <!-- C value--->
          ${get_tex_to_svg(A/2-get_digits(fv(sides.c.side))*7.5 - horizontal_shift, h+10,tg("c","side",sides),undefined,undefined,base_color)}
          <g transform="translate(-100,-30)">
            ${get_tex_to_svg(A, h,tg("a","angle",sides),undefined,"1.3em",alpha_color)}
          </g>
          <!-- beta value--->
          <g transform="translate(30,-30)">
            ${get_tex_to_svg(0, h,tg("b","angle",sides),undefined,"1.3em",beta_color)}
          </g>
          <!-- h value--->
          <g transform="translate(30,-30)">
            ${get_tex_to_svg(A/2+get_digits(sides.h)*0.8 - 25, h/2+40,th(sides),undefined,"1.3em",base_color)}
          </g>

          <polygon 
                points="0,${h}  ${A},${h} ${A/2},0"
                fill="none" stroke="black" stroke-width="2"
                stroke-linejoin="round"
          />
          <g transform="translate(${angle_shift},-${angle_shift})">
              <path
                    d="
                        M ${A/2} ${h-angle_radius}
                        A ${angle_radius} ${angle_radius} , 0, 0 1, ${A/2 + angle_radius} ${h}
                        L ${A/2} ${h}
                        Z
                    "
                    fill="red" stroke="none"
              />
          </g>
          <g transform="translate(-${angle_shift},-${angle_shift})">
              <path
                    d="
                        M ${A/2-angle_radius} ${h}
                        A ${angle_radius} ${angle_radius} , 0, 0 1, ${A/2} ${h-angle_radius}
                        L ${A/2} ${h}
                        Z
                    "
                    fill="red" stroke="none"
              />
          </g>
          <line x1="${A/2}" y1="0" x2="${A/2}" y2="${h}" stroke="${base_color}" stroke-width="2" stroke-dasharray="10"/>
      </g>
      <rect width="${W}" height="${H}" style="stroke-width:0;stroke:rgb(0,0,0)" fill="none" />
    </svg>
    `
}


const step_figure_svg_dashed = (
        sides,angle_shift=2,angle_radius=20, base_color="green", horizontal_shift=5,
        gamma_color="black", a_color="black", alpha_color="black", beta_color="black"
    ) => {
    let
        x_shift = 200,
        y_shift = 120,
        A = 380,
        h = 300,
        last_dot_x = h/2.4
    let
        W = A+x_shift,
        H = h+y_shift

    return `
    <svg width="${W}" height="${H}" class="">
          <g transform="translate(${(W-A)/2},${(H-h)/2})">
          <!-- A value--->
          <g transform="translate(0,0)">
            ${get_tex_to_svg(last_dot_x/2-get_digits(fv(sides.c.side))*15 - 30, h/2-20,tg("a","side",sides),undefined,undefined,base_color)}
          </g>
          <!-- B value--->
          <g transform="translate(0,0)">
            ${get_tex_to_svg(A-(A-last_dot_x)/2+50, h/2-20,tg("b","side",sides),undefined,undefined,a_color)}
          </g>
          <!-- C1 value--->
          ${get_tex_to_svg(A/4-get_digits(sides.c.side)*7.5 - horizontal_shift, h+10,tm("c","side",sides),undefined, "1.7em",base_color)}
          <!-- C2 value--->
          ${get_tex_to_svg(A*3/4-get_digits(sides.c.side)*7.5 - horizontal_shift, h+10,tm("c","side",sides), undefined, "1.7em","black")}
          <!-- alpha value--->
          <g transform="translate(-100,-30)">
            ${get_tex_to_svg(A, h,tg("a","angle",sides),undefined,"1.3em",alpha_color)}
          </g>
          <!-- beta value--->
          <g transform="translate(30,-30)">
            ${get_tex_to_svg(0, h,tg("b","angle",sides),undefined,"1.3em",beta_color)}
          </g>
          <!-- h value--->
          <g transform="translate(30,-30)">
            ${get_tex_to_svg(A/2+get_digits(sides.h)*0.8 - 25, h/2+40,th(sides),undefined,"1.3em",base_color)}
          </g>

          <polygon 
                points="0,${h}  ${A},${h} ${A/2},0"
                fill="none" stroke="black" stroke-width="2"
                stroke-linejoin="round"
          />
          <g transform="translate(${angle_shift},-${angle_shift})">
              <path
                    d="
                        M ${A/2} ${h-angle_radius}
                        A ${angle_radius} ${angle_radius} , 0, 0 1, ${A/2 + angle_radius} ${h}
                        L ${A/2} ${h}
                        Z
                    "
                    fill="red" stroke="none"
              />
          </g>
          <g transform="translate(-${angle_shift},-${angle_shift})">
              <path
                    d="
                        M ${A/2-angle_radius} ${h}
                        A ${angle_radius} ${angle_radius} , 0, 0 1, ${A/2} ${h-angle_radius}
                        L ${A/2} ${h}
                        Z
                    "
                    fill="red" stroke="none"
              />
          </g>
          <line x1="${A/2}" y1="0" x2="${A/2}" y2="${h}" stroke="${base_color}" stroke-width="2" stroke-dasharray="10"/>
      </g>
      <rect width="${W}" height="${H}" style="stroke-width:0;stroke:rgb(0,0,0)" fill="none" />
    </svg>
    `
}

const get_guide_image = () => {
    let img_svg = document.createElement("img")
    img_svg.setAttribute("class", "svg-triangle")
    img_svg.setAttribute("alt", "triangle")
    img_svg.setAttribute("src", "assets/draw2.svg")
    return img_svg
}

const create_simetric_svg = (SIDES, base_color) => {
    let simetric_triangles = document.createElement("div"),
        sc = document.createElement("div")
    sc.setAttribute("class","step-container")
    simetric_triangles.setAttribute("class","h-triangle")
    simetric_triangles.innerHTML = step_figure_svg_dashed(SIDES, undefined, undefined, base_color,undefined,undefined,undefined,undefined,undefined)
    sc.appendChild(simetric_triangles)
    sc.appendChild(get_guide_image())
    return sc
}

const create_simetric_svg2 = (SIDES, base_color) => {
    let simetric_triangles = document.createElement("div"),
        sc = document.createElement("div")
    sc.setAttribute("class","step-container")
    simetric_triangles.setAttribute("class","h-triangle")
    simetric_triangles.innerHTML = step_figure_svg_dashed2(SIDES, undefined, undefined, base_color,undefined,undefined,undefined,undefined,undefined)
    sc.appendChild(simetric_triangles)
    return sc
}

const step_figures = (SIDES, gamma_color="black", a_color="black", h_color="black", alpha_color="black", beta_color="black",include_small_triangle=true) => {
    let
        left_figure = document.createElement("div"),
        results = document.createElement("div"),
        guide_image = get_guide_image(),
        step_container = document.createElement("div")
    step_container.setAttribute("class","step-container")
    guide_image.setAttribute("class","svg-triangle")
    left_figure.setAttribute("class","svg-result-step")
    results.setAttribute("class","tmp-results")

    left_figure.innerHTML = step_figure_svg(SIDES,undefined,undefined,undefined,undefined, gamma_color,undefined,alpha_color,beta_color)
    results.innerHTML = temp_results_generator(SIDES, gamma_color, a_color, h_color, alpha_color)

    step_container.appendChild(left_figure)
    if(include_small_triangle) {
        step_container.appendChild(guide_image)
    }
    step_container.appendChild(results)

    return step_container
}