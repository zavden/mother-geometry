const run_program = () => {
    console.clear()
    steps_block.innerHTML = ""
    let step_container = document.createElement("div")
    step_container.setAttribute("class", "root-block")
    let div_test = document.createElement("div")
    div_test.setAttribute("class","test-step")

    steps_block.appendChild(step_container)

    MathJax.typesetPromise()
    const raw_values = Array.from(all_inputs_arr,x => x.value)
    const introduced_values = raw_values.filter(x => x !== "")
    console.log(raw_values)
    console.log(introduced_values)
    let ERRORS = []
    if(introduced_values.length > 2) ERRORS.push("There is too much data")
    if(introduced_values.length < 2) ERRORS.push("There are not enough values")

    introduced_values.forEach(e => {
        if(!!return_error_value(e))
            ERRORS.push(return_error_value(e))
    })

    if(ERRORS.length > 0){
        let err_msj = ERRORS.join("\n")
        alert(err_msj)
    } else {
        //////// RUN CODE
        // console.clear()
        let SIDES = read_sides()
        console.log( JSON.stringify( SIDES ) )
        console.table( read_sides() )

        const main_dict = [SIDES.a.side, SIDES.b.side, SIDES.c.side]
        let nL = main_dict.filter(x => x !== false).length
        console.log(`nL = ${nL}`)

        // _____ _____ ____ _____ ____
        // |_   _| ____/ ___|_   _/ ___|
        // | | |  _| \___ \ | | \___ \
        // | | | |___ ___) || |  ___) |
        // |_| |_____|____/ |_| |____/
        let sides_steps = number_solution(SIDES)
        console.table(SIDES)
        let
            color_steps = ["green","orange","blue"],
            f1 = step_figures(sides_steps[0],undefined,undefined,undefined,color_steps[0],color_steps[0]),
            f2 = step_figures(sides_steps[1], color_steps[1], undefined, undefined, undefined, undefined),
            f3 = step_figures(
                sides_steps[2],undefined,undefined, undefined,
                undefined, undefined, false)

        // TEX STEP 1
        let tex_step1 = document.createElement("div"),
            tex_step2 = document.createElement("div"),
            tex_step3 = document.createElement("div"),
            tex_step4 = document.createElement("div")
        tex_step1.innerHTML = sum_angles(SIDES.a.angle, SIDES.b.angle, SIDES.c.angle,color_steps[0])
        tex_step2.innerHTML = sine_theorem(SIDES.a.side, SIDES.a.angle, SIDES.c.side, SIDES.c.angle,color_steps[1])
        tex_step3.innerHTML = pythagorean_theorem(SIDES.h, SIDES.b.side, SIDES.c.side,color_steps[0])
        tex_step4.innerHTML = area_solution(SIDES.Area,SIDES.c.side, SIDES.h,color_steps[2])
        for(let tmp of [tex_step1, tex_step2, tex_step3, tex_step4]){
            tmp.setAttribute("class","tex-step")
        }
        // STEP 1
        step_container.appendChild(f1)
        step_container.appendChild(tex_step1)
        step_container.appendChild(document.createElement("hr"))
        // STEP 2
        step_container.appendChild(f2)
        step_container.appendChild(tex_step2)
        step_container.appendChild(document.createElement("hr"))
        // STEP 3
        step_container.appendChild(f3)
        step_container.appendChild(middle_title())
        step_container.appendChild(create_simetric_svg(sides_steps[2],color_steps[0]))
        step_container.appendChild(tex_step3)
        step_container.appendChild(document.createElement("hr"))
        // STEP 4
        step_container.appendChild(create_simetric_svg2(sides_steps[4],color_steps[2]))
        step_container.appendChild(tex_step4)
    }

    MathJax.typesetPromise()
}