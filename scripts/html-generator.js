const temp_results_generator = (RESULTS, gamma_color="black", a_color="black", h_color="black", alpha_color="black") => {
    return `
        <p class="p-result">\\(\\alpha= ${tg("a","angle",RESULTS)} \\)</p>
        <p class="p-result">\\(\\beta= ${tg("b","angle",RESULTS)} \\)</p>
        <p class="p-result">\\({\\gamma= ${tg("c","angle",RESULTS)} }\\)</p>
        <p class="p-result">\\({a= ${tg("a","side",RESULTS)} }\\)</p>
        <p class="p-result">\\({b= ${tg("b","side",RESULTS)} }\\)</p>
        <p class="p-result">\\(c= ${tg("c","side",RESULTS)} \\)</p>
        <p class="p-result">\\({h= ${th(RESULTS)} }\\)</p>
        <p class="p-result">\\(A= ${ta(RESULTS)} \\)</p>
    `
}